Cryton Frontend provides functionality for interacting with Cryton Core more easily and clearly than by using CLI.

## Listing data
You can list all Cryton data by using list pages in the navigation bar. Most important data can be found directly in 
the dashboard. Each data table provides functionality for sorting and filtering data.

## Creating objects
You can create workers, templates, instances, and runs by using create pages in the navigation bar. Every object can be 
also deleted from its list page.

## Template creation
You can create plan templates in the **Plan templates > Create template**. The whole creation process is documented 
in-app with an introduction page and additional help pages for every creation step.

## Run interaction
The front end provides 2 ways to interact with runs. There is a quick interaction menu that you can access in 
**Runs > List runs** by clicking on a run. The interaction menu will expand under the run. Another way is to click 
on the eye icon next to the run which will take you to the run's page. There you can also view the current state of the run 
and its sub-parts, and modify the execution variables for each execution.

### Execution timeline
You can view timelines of the run's executions by clicking on the clock icon next to a run on the list runs page or by 
clicking on the **show timeline** button on the run's page. The timeline shows the start, pause and finish times of the whole 
execution, stages, and steps. More details can be found in an in-app help page found inside the timeline tab.

## Theming
The front end provides two color themes - light and dark. You can switch between them with a toggle button in the top bar.
