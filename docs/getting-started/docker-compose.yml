version: '3.9'
services:
  cryton_core:
    restart: always
    image: registry.gitlab.ics.muni.cz:443/cryton/cryton-core:{{{ release_version }}}
    container_name: cryton-core
    ports:
      - "127.0.0.1:8000:80"
    env_file:
      - .env
    depends_on:
      cryton_pgbouncer:
        condition: service_started
      cryton_rabbit:
        condition: service_healthy

  cryton_proxy:
    restart: always
    image: registry.gitlab.ics.muni.cz:443/cryton/cryton-core:proxy-{{{ release_version }}}
    container_name: cryton-proxy
    network_mode: service:cryton_core
    depends_on:
      cryton_core:
        condition: service_started

  cryton_cli:
    restart: always
    image: registry.gitlab.ics.muni.cz:443/cryton/cryton-cli:{{{ release_version }}}
    container_name: cryton-cli
    network_mode: service:cryton_core
    env_file:
      - .env
    depends_on:
      cryton_core:
        condition: service_started
    tty: true

  cryton_frontend:
    restart: always
    image: registry.gitlab.ics.muni.cz:443/cryton/cryton-frontend:{{{ release_version }}}
    container_name: cryton-frontend
    ports:
      - "127.0.0.1:8080:80"

  cryton_worker:
    restart: always
    image: registry.gitlab.ics.muni.cz:443/cryton/cryton-worker:kali-{{{ release_version }}}
    container_name: cryton-worker
    network_mode: host
    env_file:
      - .env

  cryton_empire:
    restart: always
    image: bcsecurity/empire:v{{{ empire_version }}}
    container_name: cryton-empire
    network_mode: host
    env_file:
      - .env
    stdin_open: true
    command: [ "server", "--username", "$CRYTON_WORKER_EMPIRE_USERNAME", "--password", "$CRYTON_WORKER_EMPIRE_PASSWORD" ]

  cryton_msf:
    restart: always
    image: registry.gitlab.ics.muni.cz:443/cryton/configurations/metasploit-framework:0
    container_name: cryton-msf
    network_mode: host
    env_file:
      - .env
    environment:
      MSF_DB_NAME: $CRYTON_CORE_DB_NAME
      MSF_DB_USERNAME: $CRYTON_CORE_DB_USERNAME
      MSF_DB_PASSWORD: $CRYTON_CORE_DB_PASSWORD
      MSF_DB_PREPARED_STATEMENTS: false
      MSF_DB_ADVISORY_LOCKS: false
    tty: true
    depends_on:
      cryton_pgbouncer:
        condition: service_started

  cryton_db:
    restart: always
    image: postgres:15
    container_name: cryton-db
    env_file:
      - .env
    environment:
      POSTGRES_PASSWORD: $CRYTON_CORE_DB_PASSWORD
      POSTGRES_USER: $CRYTON_CORE_DB_USERNAME
      POSTGRES_DB: $CRYTON_CORE_DB_NAME
      POSTGRES_HOST_AUTH_METHOD: md5
    volumes:
      - cryton_db_data:/var/lib/postgresql/data
    healthcheck:
      test: /usr/bin/pg_isready -U $$POSTGRES_USER
      interval: 20s
      timeout: 10s
      retries: 5

  cryton_pgbouncer:
    restart: always
    image: bitnami/pgbouncer:1.18.0
    container_name: cryton-pgbouncer
    depends_on:
      cryton_db:
        condition: service_healthy
    env_file:
      - .env
    environment:
      POSTGRESQL_HOST: cryton_db
      POSTGRESQL_USERNAME: $CRYTON_CORE_DB_USERNAME
      POSTGRESQL_DATABASE: $CRYTON_CORE_DB_NAME
      POSTGRESQL_PASSWORD: $CRYTON_CORE_DB_PASSWORD
      PGBOUNCER_DATABASE: $CRYTON_CORE_DB_NAME
      PGBOUNCER_PORT: 5432
      PGBOUNCER_MAX_CLIENT_CONN: 1000
      PGBOUNCER_MIN_POOL_SIZE: 8
      PGBOUNCER_POOL_MODE: transaction
    ports:
      - "127.0.0.1:5432:5432"

  cryton_rabbit:
    restart: always
    image: rabbitmq:3.11-management
    container_name: cryton-rabbit
    env_file:
      - .env
    environment:
      RABBITMQ_DEFAULT_USER: $CRYTON_CORE_RABBIT_USERNAME
      RABBITMQ_DEFAULT_PASS: $CRYTON_CORE_RABBIT_PASSWORD
    ports:
      - "5672:5672"
      - "127.0.0.1:15672:15672"
    healthcheck:
      test: rabbitmqctl eval '
        { true, rabbit_app_booted_and_running } = { rabbit:is_booted(node()), rabbit_app_booted_and_running },
        { [], no_alarms } = { rabbit:alarms(), no_alarms },
        [] /= rabbit_networking:active_listeners(),
        rabbitmq_node_is_healthy.
        ' || exit 1
      interval: 20s
      timeout: 10s
      retries: 5

volumes:
  cryton_db_data:
